#EE513: Assignment 1 2019/2020

#Interfacing Embedded Systems to the Real World: 
#you are required to develop a self-contained embedded system that 
#interfaces to the real world using electronic sensors. High-level 
#software must be written to wrap the low-level interface.
#This assignment is worth 10% of the overall module mark for EE513.

#author: Derek Aherne
#student number:
#email: derek.aherne6@mail.dcu.ie

#date: 08/03/2020

#Hardware:
## Raspberry Pi 3 Model B v1.2
## HDMI monitor
## USB Keyboard and mouse
## Micro USB charger
## DS3231 RTC
## ADXL345 



